/**
 * Empty object for groups in scenegraph.
 *
 * @namespace cog1.data
 * @module empty
 */
define(["exports", "data"], function(exports, data) {
	"use strict";

	/**
	 * Create an instance of the model defined in this module.
	 *
	 * @parameter object with fields:
	 * @returns instance of this model.
	 */
	exports.create = function(parameter) {

    if (parameter) {
      var scale = parameter.scale;
    }

    if (scale == undefined) {
      scale = 200;
    }

		// Instance of the model to be returned.
		var instance = {};

		instance.vertices = [

      //Grundgerüst
      [-1,-1,2], //0
      [1,-1,2], //1
      [1,-1,-2], //2
      [-1,-1,-2], //3

      [-1,1,2], //4
      [1,1,2], //5
      [1,1,-2], //6
      [-1,1,-2], //7

			//Dach
			[0,2,2], //8
			[0,2,-2], //9

			//Haustür zwischen 0,1 & 4,5
			[-0.5,-1,2], //10
			[0.5,-1,2], //11
			[-0.5,0.25,2], //12
			[0.5,0.25,2], //13

			//Doppelfenster ziwschen 1,2 & 5,6
			[1,-0.5,1], //14
			[1,-0.5,-1], //15
			[1,0.5,1], //16
			[1,0.5,-1], //17

			[1,-0.45,0], //18
			[1,0.45,-0], //19

			//Fenster zwischen 2,3 & 6,7
			[0.5,-0.5,-2], //20
			[-0.5,-0.5,-2], //21
			[0.5,0.5,-2], //22
			[-0.5,0.5,-2], //23

			//Balkon zwischen 0,3 & 4,7
			[-1,-0.35,1],//24
			[-1,-0.35,-1], //25
			[-1,0.35,1], //26
			[-1,0.35,-1], //27

			[-2,-0.35,0.5],//28
			[-2,-0.35,-0.5], //29
			[-2,0.35,0.5], //30
			[-2,0.35,-0.5] //31

    ];

		instance.polygonVertices = [

      [3,2,1,0],
      [4,5,6,7],
      [4,0,1,5],
      [1,2,6,5],
      [6,2,3,7],
      [3,0,4,7],
			[4,8,5],
			[7,9,6],
			[8,9],
			[10,11,13,12],
			[14,15,17,16],
			[18,19],
			[20,21,23,22],
			[24,25,27,26],
			[28,29,31,30],
			[24,28],
			[25,29],
			[26,30],
			[27,31]
    ];

		instance.polygonColors = [0,1,2,3,2,3,4,4,4,6,6,6,6,6,6,6,6,6,6,6,6,6];

    data.applyScale.call(instance, scale);

		return instance;
	};
});
