/**
 * Populate the scene-graph with nodes,
 * calling methods form the scene-graph and node modules.
 *
 * Texture files have to exist in the "textures" sub-directory.
 *
 * @namespace cog1
 * @module createScene
 */
define(["exports", "scenegraph", "animation"], //
function(exports, scenegraph, animation) {
    "use strict";

	/**
	 * 	Call methods form the scene-graph (tree) module to create the scene.
	 *
	 */
	function init() {

    var sphere = scenegraph.createNodeWithModel("sphere", "sphere", {recurstionDepth: 3, scale: 300});

    // BEGIN exercise Scenegraph

		// Set parent-child relationships for scene-graph nodes.
    /*var shoulder = scenegraph.createNodeWithModel("oberarm", "sphere", {recurstionDepth: 2, scale:80});


    var upperArm = scenegraph.createNodeWithModel("oberarm", "cube", {scale:50}, shoulder);
    upperArm.scale([3, 0.05, 0.05]);
    upperArm.translate([96, -248, 0]);
    upperArm.rotate([0, 0, -1.2]);

    var elbow = scenegraph.createNodeWithModel("ellenbogen", "sphere", {recurstionDepth: 2, scale:60}, shoulder);
    elbow.translate([180, -464, 0]);

    var lowerArm = scenegraph.createNodeWithModel("unterarm", "cube", {scale:35}, elbow);
    lowerArm.scale([5, 0, 0]);
    lowerArm.translate([246, -4, 0]);

    var wrist = scenegraph.createNodeWithModel("handgelenk", "sphere", {recurstionDepth: 2, scale:40}, elbow);
    wrist.translate([474, -4, 0]);
    wrist.rotate([0.8, 0.3, 0]);

    var hand = scenegraph.createNodeWithModel("hand", "cube", {scale:40}, wrist);
    hand.scale([0.7, 1.4, -0.7]);
    hand.translate([110, 0, 0]);
    hand.rotate([0, 0, -1.57]);*/

    // END exercise Scenegraph


		// Set the initially interactive node [by name].
		// If not set, it is the first node created.
		//scenegraph.setInteractiveNodeByName("sphere");
		//scenegraph.setInteractiveNode(torusNode);

		// Create a node for the light, which is not visible by default.
		var lightnode = scenegraph.createPointLightNode("light", "diamond");

		// Set light parameter.
		// ambientLI, pointLI, pointPos, specularLI, specularLIExpo
		scenegraph.setLights(0.5, 0.6, [200, 200, 300], 4.0, 10);
	}

	// Public API.
	exports.init = init;
});
